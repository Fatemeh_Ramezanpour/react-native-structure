import axios from 'axios';
// import ApiConstants from './ApiConstants';
export default function api(path, method, token) {
  // let options;
  // options = {
  //   headers: {
  //     Accept: 'application/json',
  //     'Content-Type': 'application/json',
  //     ...(token && { token: token }),
  //   },
  //   method: method,
  //   ...(params && { body: JSON.stringify(params) }),
  //   url: ApiConstants.BASE_URL + path
  // };

  // return axios(options)
  //   .then(resp => resp.json())
  //   .then(json => json)
  //   .catch(error => error);

  return axios({
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...(token && { token: token }),
    },
    method: method,
    url: path,
    // data: {
    //   firstName: 'Fred',
    //   lastName: 'Flintstone'
    // }
  });
}

