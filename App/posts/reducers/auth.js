import {
  LOAD_USER_INFO,
  LOAD_USER_INFO_SUCCESS,
  SET_TOKEN,
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  LOGOUT_SUCCESS,
  LOGOUT_FAIL,
  OTP_CREATION,
  OTP_CREATION_SUCCESS,
  OTP_CREATION_FAIL,
  VERIFY_MOBILE,
  VERIFY_MOBILE_SUCCESS,
  VERIFY_MOBILE_FAIL,
  CHANGE_PASSWORD,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_FAIL,
  LOAD_USER_INFO_FAIL
} from "../actions/authAction";

export const initialState = {
  user: null,
  token: null,
  loading: false,
  verifyMobile: null,
  verifyLoading: false,
  OTP: false,
};

export default function auth(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        loading: true,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        user: action.data.profile,
        token: action.data.token,
        loading: false,
      };
    case LOGIN_FAIL:
      return {
        ...state,
        user: action.err,
        loading: false,
      };
    case LOAD_USER_INFO:
      return {
        ...state,
        ...{ user: null },
      };
    case LOAD_USER_INFO_SUCCESS:
      // console.log("action.data",action.data)
      return {
        ...state,
        ...{ data: action.data },
      };
    case LOAD_USER_INFO_FAIL:
      // console.log("action.data",action.data)
      return {
        ...state,
        // ...{ user: action.data },
      };
    case SET_TOKEN:
      return {
        ...state,
        token: action.data,
      };
    case LOGOUT:
      return {
        logoutLoading: true,
      };
    case LOGOUT_SUCCESS:
      return {
        user: null,
        token: null,
        logoutLoading: false,
      };
    case LOGOUT_FAIL:
      return {
        ...state,
        logoutLoading: false,
        error: action.err,
      };
    case OTP_CREATION:
      return {
        ...state,
        verifyLoading: true,
        OTP: false,
      };
    case OTP_CREATION_SUCCESS:
      return {
        ...state,
        verifyMobile: action.data,
        OTP: true,
        verifyLoading: false,
      };
    case OTP_CREATION_FAIL:
      return {
        ...state,
        verifyMobile: action.err,
        verifyLoading: false,
        OTP: false,
      };
    case VERIFY_MOBILE:
      return {
        ...state,
        loading: true,
      };
    case VERIFY_MOBILE_SUCCESS:
      return {
        ...state,
        user: action.data.profile,
        token: action.data.token,
        loading: false,
      };
    case VERIFY_MOBILE_FAIL:
      return {
        ...state,
        error: action.err,
        loading: false,
      };
    case CHANGE_PASSWORD:
      return {
        ...state,
        loading: true,
      };
    case CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        loading: false,
        changePass: action.data,
      };
    case CHANGE_PASSWORD_FAIL:
      return {
        ...state,
        loading: false,
        changePass: null,
        error: action.err,
      };
    default:
      return state;
  }
}
