import { combineReducers } from "redux";
// import { reducer as formReducer } from "redux-form";
import auth from "./auth";
// import movie from "./movie";
// import dialogue from "./dialogue";
// import comments from "./commentsReducer";
// import criticArchive from "./criticArchive";
// import menu from "./menu";
// import alert from "./alert";
// import search from "./search";
// import modal from "./modal";
// import profile from "./profile";
// import news from "./news";
// import download from "./download";
// import video from "./video";
// import critic from "./critic";

export default combineReducers({
  // form: formReducer,
  auth,
  // movie,
  // dialogue,
  // comments,
  // criticArchive,
  // menu,
  // alert,
  // search,
  // modal,
  // profile,
  // news,
  // download,
  // video,
  // critic,
});
