// import "isomorphic-unfetch";

import {
  LOAD_USER_INFO,
  loadUserinfoSuccess,
  loadUserinfoFail
} from "../actions/authAction";
// import { addError } from "posts/actions/alertAction";

import { call, fork, put, takeEvery } from "redux-saga/effects";

// import { Router } from "routes/routes";
// import { verifyMobileUrl, changePasswordUrl } from "services/url";

import api from "../../services";
// import es6promise from "es6-promise";
// import { OTPCreationAction } from "../actions/authAction";

// es6promise.polyfill();


function* userInfo() {
  try {
    
    const url = "https://www.salamcinama.ir/api/share/v2/download/downloadable_movies.json";
    const res = yield call(api, url, 'get', null);
    // const res = yield call(axios, url);
    yield put(loadUserinfoSuccess(res.data));
  } catch (err) {
    yield put(loadUserinfoFail(err));
  }
}

function* watchLoadUserInfo() {
  yield takeEvery(LOAD_USER_INFO, userInfo);
}
export default function* authSagas() {
  yield fork(watchLoadUserInfo);
}
