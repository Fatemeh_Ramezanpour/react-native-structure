import { all } from "redux-saga/effects";
import authSagas from "./authSaga";
// import movieSagas from "./movieSaga";
// import dialogueSagas from "./dialogue";
// import commentsSagas from "./commentsSaga";
// import criticArchiveSagas from "./criticArchiveSaga";
// import rateSagas from "./rateSaga";
// import searchSagas from "./searchSaga";
// import profileSagas from "./profileSaga";
// import newsSagas from "./newsSaga";
// import downloadSagas from "./downloadSaga";
// import videoSagas from "./videoSaga";
// import criticSagas from "./criticSaga";

export default function* rootSaga() {
  yield all([
    authSagas(),
    // movieSagas(),
    // dialogueSagas(),
    // commentsSagas(),
    // criticArchiveSagas(),
    // rateSagas(),
    // searchSagas(),
    // profileSagas(),
    // newsSagas(),
    // downloadSagas(),
    // videoSagas(),
    // criticSagas(),
  ]);
}
