export const LOAD_USER_INFO = "auth/LOAD_USER_INFO";
export const LOAD_USER_INFO_SUCCESS = "auth/LOAD_USER_INFO_SUCCESS";
export const SET_TOKEN = "auth/SET_TOKEN";
export const REMOVE_TOKEN = "auth/REMOVE_TOKEN";

export const LOGIN = "auth/LOGIN";
export const LOGIN_SUCCESS = "auth/LOGIN_SUCCESS";
export const LOGIN_FAIL = "auth/LOGIN_FAIL";

export const LOGOUT = "auth/LOGOUT";
export const LOGOUT_SUCCESS = "auth/LOGOUT_SUCCESS";
export const LOGOUT_FAIL = "auth/LOGOUT_FAIL";

export const OTP_CREATION = "auth/OTP_CREATION";
export const OTP_CREATION_SUCCESS = "auth/OTP_CREATION_SUCCESS";
export const OTP_CREATION_FAIL = "auth/OTP_CREATION_FAIL";

export const VERIFY_MOBILE = "auth/VERIFY_MOBILE";
export const VERIFY_MOBILE_SUCCESS = "auth/VERIFY_MOBILE_SUCCESS";
export const VERIFY_MOBILE_FAIL = "auth/VERIFY_MOBILE_FAIL";

export const CHANGE_PASSWORD = "auth/CHANGE_PASSWORD";
export const CHANGE_PASSWORD_SUCCESS = "auth/CHANGE_PASSWORD_SUCCESS";
export const CHANGE_PASSWORD_FAIL = "auth/CHANGE_PASSWORD_FAIL";
export const LOAD_USER_INFO_FAIL = "auth/LOAD_USER_INFO_FAIL";

//login
export function loginAction(data, params) {
  return {
    type: LOGIN,
    data,
    params,
  };
}
export function loginActionSuccess(data) {
  return {
    type: LOGIN_SUCCESS,
    data,
  };
}
export function loginActionFailure(err) {
  return {
    type: LOGIN_FAIL,
    err,
  };
}

// call profile api
export function loadUserinfo() {
  return {
    type: LOAD_USER_INFO,
  };
}
export function loadUserinfoSuccess(data) {
  return {
    type: LOAD_USER_INFO_SUCCESS,
    data,
  };
}
export function loadUserinfoFail(err) {
  return {
    type: LOAD_USER_INFO_FAIL,
    err,
  };
}

export function setToken(data) {
  return {
    type: SET_TOKEN,
    data,
  };
}

//logout
export function logOutAction() {
  return {
    type: LOGOUT,
  };
}
export function logoutActionSuccess(data) {
  return {
    type: LOGOUT_SUCCESS,
    data,
  };
}
export function logoutActionFailure(err) {
  return {
    type: LOGOUT_FAIL,
    err,
  };
}

// once time password create
export function OTPCreationAction(params, data) {
  return {
    type: OTP_CREATION,
    params,
    data,
  };
}
export function OTPCreationActionSuccess(data) {
  return {
    type: OTP_CREATION_SUCCESS,
    data,
  };
}
export function OTPCreationActionFailure(err) {
  return {
    type: OTP_CREATION_FAIL,
    err,
  };
}

//mobile-verification
export function verifyMobileAction(params, mobile, code) {
  return {
    type: VERIFY_MOBILE,
    params,
    mobile,
    code,
  };
}
export function verifyMobileActionSuccess(data) {
  return {
    type: VERIFY_MOBILE_SUCCESS,
    data,
  };
}
export function verifyMobileActionFailure(err) {
  return {
    type: VERIFY_MOBILE_FAIL,
    err,
  };
}

//change password
export function changePasswordAction(params, data) {
  return {
    type: CHANGE_PASSWORD,
    params,
    data,
  };
}

export function changePasswordActiondSuccess(data) {
  return {
    type: CHANGE_PASSWORD_SUCCESS,
    data,
  };
}

export function changePasswordActionFailure(err) {
  return {
    type: CHANGE_PASSWORD_FAIL,
    err,
  };
}
