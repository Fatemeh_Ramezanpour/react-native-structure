import { createStore, applyMiddleware } from 'redux';
import rootReducer from './posts/reducers/rootReducer';
import rootSaga from "./posts/sagas/index";
import { persistReducer, persistStore } from 'redux-persist';
// import { composeWithDevTools } from 'remote-redux-devtools';
import AsyncStorage from '@react-native-community/async-storage';

import createSagaMiddleware from "redux-saga";
const sagaMiddleware = createSagaMiddleware();

const bindMiddleware = middleware => {
  // if (process.env.NODE_ENV !== "production") {
    const { composeWithDevTools } = require('redux-devtools-extension');
    return composeWithDevTools(applyMiddleware(...middleware))
  // }

  // return applyMiddleware(...middleware);
};

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  /**
   * Blacklist state that we do not need/want to persist
   */
  blacklist: [
    // 'auth',
  ],
}
// Redux persist
const persistedReducer = persistReducer(persistConfig, rootReducer)

function configureStore(initialState) {
  const store = createStore(
    persistedReducer,
    initialState,
    bindMiddleware([sagaMiddleware])
  );
  const persistor = persistStore(store)

  store.runSagaTask = () => {
    store.sagaTask = sagaMiddleware.run(rootSaga);
  };

  store.runSagaTask();
  return {store, persistor} ;
}
export default configureStore;


// const persistConfig = {
//   key: 'root',
//   storage: storage,
//   whitelist: ['session']
// };

// const sagaMiddleware = createSagaMiddleware()

// const persistedReducer = persistReducer(persistConfig, rootReducer)
// const store = createStore(
//   persistedReducer,
//   applyMiddleware(sagaMiddleware)
// )
// const persistor = persistStore(store)

// sagaMiddleware.run(rootSaga)