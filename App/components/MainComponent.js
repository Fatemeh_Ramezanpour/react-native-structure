import React from 'react';
import { connect } from "react-redux";

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  Button
} from 'react-native';
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { loadUserinfo } from "../posts/actions/authAction";
// const MainComponent: () => React$Node = () => {
  class MainComponent extends React.Component{
    componentDidMount() {
      // console.log("this.props",this.props)
      this.props.dispatch(loadUserinfo());

    }
    render(){
      const { data } = this.props;
      console.log("data",data);
      let pic = {
          uri: 'https://api.salamcinama.ir/uploads/movie/poster/4472/small_ca1b066d-fe02-4826-83c7-59988f255267.jpg'
        };
        let items=[];
        if(data && data.data && data.data.length > 0){
          items = data.data.map((item) => {
            return(
              <View style={styles.viewAlign}>
                <Image source={{uri:item.poster.large}} style={styles.imageFormat}/>
                <Text style={styles.movieName}>{item.name}</Text>
                <Button
                  title="خرید"
                  color="#ff00"
                  style={styles.btn}
                  onPress={() => Alert.alert('Button with adjusted color pressed')}
                />
              </View>
            )
          })
        }
        console.log("items", data.data[1].poster.small);
      return (
        <>
          <StatusBar barStyle="dark-content" />
          <SafeAreaView>
            <Header />
            <Text>Social Movie</Text>
            <ScrollView
              horizontal={true}
              contentInsetAdjustmentBehavior="automatic"
              alwaysBounceHorizontal={true}
              style={styles.scrollView}>
              {/* {global.HermesInternal == null ? null : (
                <View style={styles.engine}>
                  <Text style={styles.footer}>Engine: Hermes</Text>
                </View>
              )} */}
              {items}
              {/* <View style={styles.body}>
                <View style={styles.sectionContainer}>
                  <Text style={styles.sectionDescription}>
                    Edit <Text style={styles.highlight}>App.js</Text> to change this
                    Salam Fatemeh.Ramezanpour
                  </Text>
                </View>
              </View> */}
    
            </ScrollView>
          </SafeAreaView>
          
        </>
      );
    }
  }

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.dark,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.dark,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  imageFormat: {
    borderRadius: 10,
    width: 200,
    height: 250,
    zIndex:999
  },
  viewAlign: {
    paddingRight: 10,
    justifyContent: 'center', 
    alignItems: 'center' 
  },
  movieName:{
    fontSize: 18,
    fontWeight: '300',
    color: Colors.white,
  },
  btn:{
    padding: 10
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});
const mapStateToProps = state => ({
  data: state.auth
});
// const mapDispatchToProps = dispatch => ({
//   movie: () => dispatch(loadUserinfo()),
// });

export default connect(mapStateToProps)(MainComponent);