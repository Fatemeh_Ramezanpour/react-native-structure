/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react'
import MainComponent from "./App/components/MainComponent"
import configureStore from './App/store.js';
const { store, persistor } = configureStore();

const App = () => (
  <Provider store = { store }>
    <PersistGate loading={null} persistor={persistor}>
      <MainComponent />
    </PersistGate>
  </Provider>
)

export default App;
